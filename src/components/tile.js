import React from "react"

export const Tile = ({ label, src, yellow,margin,href }) => {
  return (
    <div className={`tile--container ${yellow && "yellow"}`}>
      <label className="tile--container--label">{label}</label>
      <img className={`tile--container--icon ${margin && "margin"}`} src={src} />
      <a
        href={href}
        className={`tile--container--button ${yellow && "yellow"}`}
      >
        Wybierz
      </a>
    </div>
  )
}
