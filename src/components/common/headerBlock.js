import React from 'react'

export const HeaderBlock = ({themeColor,label}) =>{
  return <div className={`headerBlock__container ${themeColor}`}>
    <h1 className="headerBlock__container--label">
      {label}
    </h1>
  </div>
}