import React, { useEffect, useState } from "react"
import { Link } from "gatsby"
import { Sling as Hamburger } from "hamburger-react"

export const Navbar = ({ themeColor }) => {
  const [isOpen, setOpen] = useState(false)

  const [width, setWidth] = useState()
  console.log(width)

  const isDesktop = width >= 992

  useEffect(() => {
    setWidth(window.innerWidth)
  }, [])

  console.log(themeColor)

  const theme = themeColor === "yellow" ? "uslugibudowlane" : "autopomoc"

  return (
    <>
      <div className={`mobileNavbar ${isOpen && "active"} ${themeColor}`}>
        <Link
          onClick={() => setOpen(false)}
          className="nav--container--menu--link"
          activeClassName="active"
          to={`/${theme}/`}
        >
          Strona główna
        </Link>
        <Link
          onClick={() => setOpen(false)}
          className="nav--container--menu--link"
          activeClassName="active"
          to={`/${theme}/oferta`}
        >
          Oferta
        </Link>
        <Link
          onClick={() => setOpen(false)}
          className="nav--container--menu--link"
          activeClassName="active"
          to={`/${theme}/kontakt`}
        >
          Kontakt
        </Link>
      </div>
      <nav className={`nav--container ${themeColor}`}>
        <div className="nav--container--content">
          <a href="/">
            <img
              className="nav--container--content--logo"
              src={`/images/logo${themeColor}.svg`}
            />
          </a>
          {isDesktop ? (
            <ul className="nav--container--menu">
              <Link
                className="nav--container--menu--link"
                activeClassName="active"
                to={`/${theme}/`}
              >
                Strona główna
              </Link>
              <Link
                className="nav--container--menu--link"
                activeClassName="active"
                to={`/${theme}/oferta`}
              >
                Oferta
              </Link>
              <Link
                className="nav--container--menu--link"
                activeClassName="active"
                to={`/${theme}/kontakt`}
              >
                Kontakt
              </Link>
            </ul>
          ) : (
            <Hamburger
              toggled={isOpen}
              toggle={setOpen}
              color={themeColor === "yellow" ? "#fedb00" : "#f36e21"}
              size={24}
            />
          )}
        </div>
      </nav>
    </>
  )
}
