import React from "react"
import { Navbar } from "./navbar"
import { Footer } from "./footer"


const Layout = ({ children,themeColor }) => {
  return (
    <>
      <Navbar themeColor={themeColor}/>
      <>{children}</>
      <Footer themeColor={themeColor}/>
    </>
  )
}


export default Layout
