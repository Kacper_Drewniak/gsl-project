import React from "react"
import Layout from "../../components/layout"
import { HeaderBlock } from "../../components/common/headerBlock"
import { LogoWithText } from "../../components/oferta/logoWithText"
import { DoubleList } from "../../components/common/doubleList"
import { Gallery } from "../../components/oferta/gallery"
import { ContactSection } from "../../components/common/contactSection"

const Oferta = () => {
  return (
    <Layout themeColor="orange">
      <HeaderBlock themeColor="orange" label="oferta" />
      <LogoWithText
        themeColor="orange"
        title="Firma GSL"
        text="świadczy usługi pomocy drogowej oraz transportu na terenie Polski
i Europy. Specjalizujemy się w transporcie nowych i używanych samochodów, maszyn budowlanych oraz rolniczych. Współpracujemy
z firmami, jak i z osobami prywatnymi
"
      />
      <DoubleList/>
      <Gallery/>
      <ContactSection/>
    </Layout>
  )
}

export default Oferta
