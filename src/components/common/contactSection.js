import React from "react"

export const ContactSection = ({ themeColor }) => {

    const theme = themeColor === "yellow" ?  "uslugibudowlane": "autopomoc"

  return (
    <section className="contactSection__container">
      <h1 className="contactSection__container__title">Dane Firmy:</h1>
      <p className="contactSection__container__text">GSL ANNA SZLACHETKA</p>
      <p className="contactSection__container__text">ul. Wczasowa 34B</p>

      <p className="contactSection__container__text">43-300 Bielsko Biała</p>
      <p className="contactSection__container__text">NIP 9372685536</p>
      <a
        className={`contactSection__container__href  ${themeColor} `}
        href={`/${theme}/kontakt`}
      >
        Kontakt
      </a>
    </section>
  )
}
