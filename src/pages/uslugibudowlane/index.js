import React from "react"

import Layout from "../../components/layout"
import { MainImage } from "../../components/common/mainImage"
const IndexPage = () => {
  return <Layout themeColor="yellow">
    <MainImage  themeColor="yellow"/>
  </Layout>
}

export default IndexPage
