import React from "react"
import Carousel from "react-elastic-carousel"
import ImageGallery from "react-image-gallery"

export const Gallery = ({themeColor}) => {
  const images = [
    { original: "/images/orange/5.JPG" },
    { original: "/images/orange/2.JPG" },
    { original: "/images/orange/3.JPG" },
    { original: "/images/orange/4.JPG" },
    { original: "/images/orange/1.JPG" },
    { original: "/images/orange/6.JPG" },
    { original: "/images/orange/7.JPG" },
    { original: "/images/orange/8.JPG" },
    { original: "/images/orange/9.JPG" },
    { original: "/images/orange/10.JPG" },
    { original: "/images/orange/11.JPG" },
    { original: "/images/orange/12.JPG" },
    { original: "/images/orange/13.JPG" },
    { original: "/images/orange/14.JPG" },
    { original: "/images/orange/15.JPG" },
    { original: "/images/orange/16.JPG" },
    { original: "/images/orange/17.JPG" },
    { original: "/images/orange/18.JPG" },
    { original: "/images/orange/19.JPG" },
    { original: "/images/orange/20.JPG" },
    { original: "/images/orange/21.JPG" },
    { original: "/images/orange/22.JPG" },
    { original: "/images/orange/23.JPG" },
    { original: "/images/orange/24.JPG" },
    { original: "/images/orange/25.JPG" },
    { original: "/images/orange/26.JPG" },
    { original: "/images/orange/27.JPG" },
  ]

  const imagesYellow = [
    { original: "/images/yellow/1.JPG" },
    { original: "/images/yellow/2.JPG" },
    { original: "/images/yellow/3.JPG" },
    { original: "/images/yellow/4.JPG" },
    { original: "/images/yellow/5.JPG" },
    { original: "/images/yellow/6.JPG" },
    { original: "/images/yellow/7.JPG" },
    { original: "/images/yellow/8.JPG" },
    { original: "/images/yellow/9.JPG" },
    { original: "/images/yellow/10.JPG" },
    { original: "/images/yellow/11.JPG" },
    { original: "/images/yellow/12.JPG" },

  ]


  return (
    <div className="galleryContainer">
      <h1 className="galleryContainer__title">Galeria</h1>
      <ImageGallery
        items={themeColor === "yellow"? imagesYellow: images} />;
    </div>
  )
}
