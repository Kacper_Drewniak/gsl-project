import React from "react"
import Layout from "../../components/layout"
import { HeaderBlock } from "../../components/common/headerBlock"
import { LogoWithText } from "../../components/oferta/logoWithText"
import { DoubleList } from "../../components/common/doubleList"
import { Gallery } from "../../components/oferta/gallery"
import { ContactSection } from "../../components/common/contactSection"

const Oferta = () => {
  return (
    <Layout themeColor="yellow">
      <HeaderBlock themeColor="yellow" label="oferta" />
      <LogoWithText
        themeColor="yellow"
        title="Firma GSL"
        text="świadczy usługi w zakresie prac ziemnych i budowlanych"
      />
      <DoubleList themeColor="yellow" />
      <Gallery  themeColor="yellow" />
      <ContactSection themeColor="yellow" />
    </Layout>
  )
}

export default Oferta
