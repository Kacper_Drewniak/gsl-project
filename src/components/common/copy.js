import React, { useState } from "react"
import { CopyToClipboard } from "react-copy-to-clipboard"
import { Alert } from "reactstrap"

export const Copy = ({ icon, copyText, themeColor, phone }) => {
  const [isOpen, setIsOpen] = useState(false)

  const runAlert = () => {
    if(!phone) {
      setIsOpen(true)
      setTimeout(() => {
        setIsOpen(false)
      }, 2000)
    }
  }

  return (
    <>
      <Alert className={themeColor} fade={true} color="primary" isOpen={isOpen}>
        Skopiowano!
      </Alert>
      <div className="copyContainer">
        <div className="d-flex">
          <img
            class="copyContainer--image"
            src={`/images/copies/${icon}.svg`}
            height="25px"
          />
          <h4 className="copyContainer--text">{copyText}</h4>
        </div>
        <CopyToClipboard onCopy={runAlert} text={copyText}>
          {!phone ? (
            <button className={`copyContainer--button ${themeColor}`}>
              Kopiuj <img src="/images/copy.svg" />
            </button>
          ) : (
            <a className={`copyContainer--button ${themeColor}`} href={`tel:${copyText}`}>Zadzwoń</a>
          )}
        </CopyToClipboard>
      </div>
    </>
  )
}
