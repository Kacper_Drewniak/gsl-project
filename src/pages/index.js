import React from "react"
import Seo from "../components/seo"
import { Container } from "reactstrap"
import { Tile } from "../components/tile"

const IndexPage = () => {
  const tiles = [
    {
      label: "AUTO POMOC",
      src: "images/towcar.svg",
      yellow: false,
      margin: true,
      href:"/autopomoc"
    },
    {
      label: "USŁUGI BUDOWLANE",
      src: "images/digger.svg",
      yellow: true,
      margin: false,
      href:'/uslugibudowlane'
    },
  ]

  return (
    <Container fluid className="mainPage--container">
      <Seo title="Główna" />
      <img className="mainPage--container--logo" src="images/logo.svg" />
      <div className="mainPage--container--tiles">
        {tiles.map(({ label, src, yellow,margin ,href}) => {
          return <Tile {...{label,src,yellow,margin,href}}/>
        })}
      </div>
    </Container>
  )
}

export default IndexPage
