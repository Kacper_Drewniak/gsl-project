import React from "react"
import { Col, Container, Row } from "reactstrap"
export const LogoWithText = ({ themeColor, title, text }) => {
  return (
    <section className="LWT__container">
      <Row className="LWT__container__content">
        <Col className="LWT__container__col" sm={6}>
          <img
            className="LWT__container__content--logo"
            src={`/images/logo${themeColor}.svg`}
          />
        </Col>
        <Col className="LWT__container__text LWT__container__col" sm={6}>
          <h4 className="LWT__container__text--title">{title}</h4>
          <p  className="LWT__container__text--text">{text}</p>
        </Col>
      </Row>
    </section>
  )
}
