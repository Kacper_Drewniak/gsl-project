import React from "react"

export const MainImage = ({ themeColor }) => {
  return (
    <div className={`mainImage--container ${themeColor}`}>
      <img
        className="mainImage--container--logo"
        src={`/images/logo${themeColor}.svg`}
      />
      <h1 className="mainImage--container--title">GOLDEN STATE LOGISTICS</h1>
      <hr className="mainImage--container--hr" />
      <h1 className="mainImage--container--subtitle">
        {themeColor === "YELLOW" ? <>Pomoc drogowa 24h</> : <>Usługi budowlane</>}</h1>
      <a className="mainImage--container--phone" href="tel:+48 500 755 704">
        <img height="25spx" src="/images/phone.png" />
        500 755 704
      </a>
    </div>
  )
}
