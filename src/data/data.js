export const offers = [
  "Holowanie aut dostawczych (wraz z przyczepami)",
  "Holowanie aut osobowych, SUV, KAMPER",
  "Usuwanie po kolizji drogowej",
  "Odpalanie auta (12V i 24V)",
  "Dowóz paliwa",
  "Transport maszyn budowlanych, rolniczych, sprzętu ciężkiego, kontenerów budowlanych oraz ładunków ponadgabarytowych (konstrukcje stalowe, maszyny, altany itp.)",
]
export const offerYellow = [
  "Wykopy pod fundamenty, piwnice,",
  "Wykopy pod przyłącza wodno-kanalizacyjne,elektryczne i gazowe",
  "Odwodnienia",
  "Drenaże",
  "Wyrównywanie terenu",
  "Wykopy pod szamba, oczyszczalnie ścieków",
  "Budowa ogrodzeń, wjazdów",
  "Budowa oczek wodnych, stawów i innych zbiorników wodnych",
  "Budowa niedużych domów, garaży, piwnic itp do stanu surowego",
]

export const ceritificates = [
  "Licencja Transportowa Krajowa",
  "Licencja Transportowa Międzynarodowa WSPÓLNOTA EUROPEJSKA",
  "Zezwolenie Na Wykonywanie Zawodu Przewoźnika Drogowego",
  "Certyfikat Kompetencji Zawodowych w  Drogowym Transporcie Rzeczy",
  "Ubezpieczenie OCP",
]
