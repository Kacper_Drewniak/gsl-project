import React from "react"
import { Col, Row } from "reactstrap"
import { ceritificates, offers,offerYellow } from "../../data/data"

export const DoubleList = ({ themeColor }) => {
  const Title = ({ label }) => (
    <h1 className="doubleListContainer__title">{label}</h1>
  )

  return (
    <div className="doubleListContainer">
      {themeColor === "yellow" ? (
        <>
          <Row className="doubleListContainer__content justify-content-center">
            <Col lg={5} sm={12}>
              <Title label="Oferujemy:" />
              <ul className="doubleListContainer__content__list">
                {offerYellow.map(label => (
                  <li className="doubleListContainer__content__list--element dot yellow">
                    {label}
                  </li>
                ))}
              </ul>
            </Col>
          </Row>
        </>
      ) : (
        <Row className="doubleListContainer__content">
          <Col lg={5} sm={12}>
            <Title label="Oferujemy:" />
            <ul className="doubleListContainer__content__list">
              {offers.map(label => (
                <li className="doubleListContainer__content__list--element dot">
                  {label}
                </li>
              ))}
            </ul>
          </Col>
          <Col sm={1}>
            <div className="doubleListContainer__content--hr" />
          </Col>
          <Col lg={5} sm={12}>
            <Title label="Certyfikaty:" />
            <ul className="doubleListContainer__content__list">
              {ceritificates.map(label => (
                <li className="doubleListContainer__content__list--element check">
                  {label}
                </li>
              ))}
            </ul>
          </Col>
        </Row>
      )}
    </div>
  )
}
