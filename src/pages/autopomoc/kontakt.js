import React from "react"
import Layout from "../../components/layout"
import { HeaderBlock } from "../../components/common/headerBlock"
import { Copy } from "../../components/common/copy"

const Kontakt = () => {
  return (
    <Layout themeColor="orange">
      <HeaderBlock themeColor="orange" label="kontakt" />
      <div className="contactContainer">
        <img
          className="contactContainer__logo"
          src={`/images/logoorange.svg`}
        />
        <Copy phone icon="phone" copyText="+48 500 755 704" />
        <Copy phone icon="phone" copyText="+48 500 655 833" />

        <Copy icon="mail" copyText="GSLROAD@gmail.com" />
        <Copy
          icon="local"
          copyText="Ul. Wczasowa 34b
43-300 Bielsko-Biała"
        />
      </div>
    </Layout>
  )
}

export default Kontakt
