import React from "react"

export const Footer = ({ themeColor }) => {
  return (
    <footer className={`footer--container ${themeColor}`}>
      <div className="footer--container--content">
        <img src="/images/logoGrey.png" height="32px" />
        <a href="tel:+48 500 755 704">
          <img src="/images/phone.svg" height="25px" />
          +48 500 755 704
        </a>
        <a href="mailto:gslroad@gmail.com">
          <img src="/images/mail.svg" height="25px" />
          GSLROAD@gmail.com
        </a>
        <a href="mailto:gslroad@gmail.com">
          <img src="/images/local.svg" height="25px" />
          Ul. Wczasowa 34b, 43-300 Bielsko-Biała
        </a>{" "}
        <a href="mailto:gslroad@gmail.com">
        © GOLDEN STATE LOGISTICS
          2022r.
        </a>
      </div>
    </footer>
  )
}
