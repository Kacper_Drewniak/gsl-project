import React from "react"

import Layout from "../../components/layout"
import { MainImage } from "../../components/common/mainImage"
const IndexPage = () => {
  return <Layout themeColor="orange">
    <MainImage  themeColor="orange"/>
  </Layout>
}

export default IndexPage
