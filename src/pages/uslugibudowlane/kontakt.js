import React from "react"
import Layout from "../../components/layout"
import { HeaderBlock } from "../../components/common/headerBlock"
import { Copy } from "../../components/common/copy"

const Kontakt = () => {
  return (
    <Layout themeColor="yellow">
      <HeaderBlock themeColor="yellow" label="kontakt" />
      <div className="contactContainer">
        <img
          className="contactContainer__logo"
          src={`/images/logoyellow.svg`}
        />
        <Copy phone themeColor="yellow" icon="phone" copyText="+48 500 755 704" />
        <Copy phone themeColor="yellow" icon="phone" copyText="+48 500 655 833" />

        <Copy themeColor="yellow" icon="mail" copyText="GSLROAD@gmail.com" />
        <Copy
          themeColor="yellow"
          icon="local"
          copyText="Ul. Wczasowa 34b
43-300 Bielsko-Biała"
        />
      </div>
    </Layout>
  )
}

export default Kontakt
